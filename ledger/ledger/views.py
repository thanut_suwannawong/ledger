from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Note

def index(request):
    # get object
    lists = Note.objects.order_by('pub_date')
    total_index = total_money()  # total money to show in index
    income_index = total_income()  # total income to show in table
    expense_index = -total_expense()  # total expense to show in table
    context = {'lists':lists,'total_index':total_index,
               'income_index':income_index,'expense_index':expense_index}
    return render(request, 'ledger/index.html',context)
    
def add_list(request):  # add note 
    # request all POST value
    new_note = request.POST['add_note']  # note's text 
    new_money = request.POST['add_money']  # money
    money_type = request.POST['money_type']  # money type (income,expense)
    time = request.POST['localtime']  # check if user wants to user timezone
    date_note = request.POST['date']  # get date from input date
    time_note = request.POST['time']  # get time from input time
    
    if time == "tz":
        time = timezone.now()
    else:
        # DatetimeField use format => dd/mm/yyyy tt:tt
        time = date_note +" "+ time_note
        
    # if money_type is expense make it minus    
    if money_type == "ex":
        new_money = -int(new_money)
    total = total_money() + int(new_money)
    
    getnote = Note(note_text=new_note,cost_value=new_money,
                   cost_total=total,pub_date=time)
    getnote.save()
    print(timezone.now())
    return HttpResponseRedirect(reverse('ledger:index'))

def edit_list(request):  # use to view edit page
    lists = Note.objects.order_by('pub_date')  # get obj. and order by date
    total_index = total_money()  # all money
    context = {'lists':lists,'total_index':total_index,}
    return render(request,'ledger/edit_page.html',context)

def del_list(request):  # delete note
    g = request.POST.getlist('note_list[]')  # get list of selected note's id
    for n in range(0,len(g)):  # use for loop to set pk for deleting
        q = Note.objects.get(pk=g[n])
        q.delete()
    # redirect to edit page
    return HttpResponseRedirect(reverse('ledger:edit_list'))
                  
def total_money():  # all money 
    total = 0  # set default to 0
    n = Note.objects.all()  # get obj.
    for note in n:  # use for loop for all note in Model
        get_money = Note.objects.get(pk=note.id)
        total += get_money.cost_value  # calculate all money
    return total 
    
def total_income():  # all income
    total = 0  # set default to 0
    n = Note.objects.all()
    for note in n:
        get_money = Note.objects.get(pk=note.id)
        if get_money.cost_value > 0:  # if variable get_money is +
            total += get_money.cost_value  # increase total value
    return total  # returnn total
    
def total_expense():  # all expense
    total = 0  # set default to 0
    n = Note.objects.all()
    for note in n:
        get_money = Note.objects.get(pk=note.id)
        if get_money.cost_value < 0:  # if variable get_money is -
            total += get_money.cost_value  # decrease total value
    return total  # # returnn total