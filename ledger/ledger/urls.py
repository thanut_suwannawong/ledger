from django.conf.urls import url

from . import views

app_name = 'ledger'
urlpatterns =[
    url(r'^$', views.index, name='index'),
    url(r'^add_list/$', views.add_list, name='add_list'),
    url(r'^edit_list/$', views.edit_list, name='edit_list'),
    url(r'^del_list/$', views.del_list, name='del_list'),
]